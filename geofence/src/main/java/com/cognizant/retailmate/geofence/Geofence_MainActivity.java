package com.cognizant.retailmate.geofence;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Geofence_MainActivity extends AppCompatActivity {


    private static final String TAG = "APIClient";

    LocationListener locationListener;

    private GoogleApiClient mGoogleApiClient = null;
    private ArrayList<Geofence> mGeofenceList = new ArrayList<>();

    Geofence_LocationData_Model[] geofenceLocationData_models;
    Gson gson;

    private Button startGeofenceMonitoring, stopGeofenceMonitoring;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.geofence_activity_main);

        startGeofenceMonitoring = (Button) findViewById(R.id.start_geo);
        startGeofenceMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartLocation();
                StartGeo();
            }
        });

        stopGeofenceMonitoring = (Button) findViewById(R.id.stop_geo);
        stopGeofenceMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StopGeo();
            }
        });

        //Setting up Google Play Services for accessing Location
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        Log.d(TAG, "Connected");
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.d(TAG, "Connection Suspended");
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d(TAG, "Connection failed");
                    }
                })
                .build();

        //Connecting to Play services
        mGoogleApiClient.connect();

        //Check for Device Location permissions based on API level
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1234);
        }

    }
    private void StartLocation() {
        Toast.makeText(this, "Starting Location Services", Toast.LENGTH_SHORT).show();
        Log.d(TAG,"LocationService Started");
        try {
            //Location service request to fetch current device location
            LocationRequest locationRequest = LocationRequest.create()
                    .setInterval(10000)
                    .setFastestInterval(5000)
                    //.setNumUpdates(5)
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            locationListener=new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.d(TAG,"Current User Location: "+location.getLatitude()+" "+location.getLongitude());
                }
            };
            //To get the current device location every time the coordinates change
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    locationRequest, locationListener/*new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            Log.d(TAG,"Current User Location: "+location.getLatitude()+" "+location.getLongitude());
                        }
                    }*/);
        }
        catch (SecurityException e){
            Log.d("Error",e.getMessage());
        }
    }

    private void StopGeo() {
        Toast.makeText(this, "Stopping All services", Toast.LENGTH_SHORT).show();
        Log.d(TAG,"Stopping Geo Location services");

        //Add all the geofence Request ID to String Array List and pass to remove geofences
        ArrayList<String> gefenceID = new ArrayList<String>();
        for(Geofence i :mGeofenceList){
            gefenceID.add(i.getRequestId());
        }
        LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient,gefenceID);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,locationListener );
    }

    private void StartGeo() {
        try {

            //For creating multiple Geofences
            populateGeofences();

           /*To add a single Geofence use below lines*/
//            Geofence geofence = new Geofence.Builder()
//                    .setRequestId(GEOFENCE_ID)                                                                    /*Unique GEOFENCE ID*/
//                    .setCircularRegion(30,-80,10000)                                                              /*Latitude,Longitude,Range in meters*/
//                    .setExpirationDuration(Geofence.NEVER_EXPIRE)                                                 /*Duration of Geofence*/
//                    .setNotificationResponsiveness(1000)                                                          /*Interval Between in each check for transition (Lower Value-More battery usage)*/
//                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)   /*Types: ENTER,EXIT and DWELL*/
//                    .build();

            GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                    .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                    .addGeofences(mGeofenceList)
                    .build();

            //Create Intent to start the geofencing transition service
            Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
            PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            if(!mGoogleApiClient.isConnected()) {
                Log.d(TAG,"Google API Not connected");
            } else {
                LocationServices.GeofencingApi.addGeofences(mGoogleApiClient,geofencingRequest,pendingIntent)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                if(status.isSuccess()) {
                                    Log.d(TAG,"Geofence Success");
                                } else {
                                    Log.d(TAG,"Geofence Error");
                                }
                            }
                        });
            }
        }
        catch (SecurityException e){
            Log.d(TAG,"Error"+e.getMessage());
        }
    }

    private void populateGeofences() {

        gson= new Gson();
        //Location Data present offline
        String resource=loadJSONFromAsset("location_data.json");

        geofenceLocationData_models =gson.fromJson(resource,Geofence_LocationData_Model[].class);

        for(int i = 0; i< geofenceLocationData_models.length; i++){

            //1000m Radius
            mGeofenceList.add( new Geofence.Builder()
                    .setRequestId(geofenceLocationData_models[i].getplace()+" 1000m")
                    .setCircularRegion(geofenceLocationData_models[i].getLat(), geofenceLocationData_models[i].getLng(),1000)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setNotificationResponsiveness(1000)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build());

            //500m Radius
            mGeofenceList.add( new Geofence.Builder()
                    .setRequestId(geofenceLocationData_models[i].getplace()+" 500m")
                    .setCircularRegion(geofenceLocationData_models[i].getLat(), geofenceLocationData_models[i].getLng(),500)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setNotificationResponsiveness(1000)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build());

            //100m Radius
            mGeofenceList.add( new Geofence.Builder()
                    .setRequestId(geofenceLocationData_models[i].getplace()+" 100m")
                    .setCircularRegion(geofenceLocationData_models[i].getLat(), geofenceLocationData_models[i].getLng(),100)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setNotificationResponsiveness(1000)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build());
        }

    }

    //Load Json from assets folder
    public String loadJSONFromAsset(String filename) {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
