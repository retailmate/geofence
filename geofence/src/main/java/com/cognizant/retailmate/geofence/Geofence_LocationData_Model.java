package com.cognizant.retailmate.geofence;

/**
 * Created by 599654 on 1/20/2017.
 */

/**
 * Model format
 * Place (String)
 * lat (Double)
 * lng (Double)
 */


public class Geofence_LocationData_Model {

    private String place;
    private double lat,lng;

    public String getplace() {
        return place;
    }

    public void setplace(String place) {
        this.place = place;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }


}
